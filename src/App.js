
import './App.css';
import Ex_Shoe_Shop from './Ex_Shoe_Shop/Ex_Shoe_Shop';
import Redux_Ex from './Redux_Ex/Redux_Ex'

function App() {
  return (
    <div className="App">
      {/* <Ex_Shoe_Shop></Ex_Shoe_Shop> */}
      <Redux_Ex></Redux_Ex>
    </div>
  );
}

export default App;

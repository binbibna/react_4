import React, { Component } from 'react';
import Cart from './Cart';
import { dataShoe } from './data_shoe';
import ListShoe from './ListShoe';

export default class Ex_Shoe_Shop extends Component {
  state = {
    listShoe: dataShoe,
    cart: [],
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {return item.id==shoe.id})
     // th 1 : sp chưa có trong giỏ hàng =>
    if(index == -1){
      let newShoe={...shoe,soLuong:1};
      cloneCart.push(newShoe);
    }else{
      cloneCart[index].soLuong++;
    }

        // th2 : sp đã có trong giỏ hàng => uppdate số lượng
    this.setState({
      cart: cloneCart,
    });
  };

  handleChaneQuantity = (id,number) => {
    let cloneCart = [...this.state.cart];
    console.log(id);
    cloneCart.forEach(function(item){
      if(Number(item.id) === id){
        item.soLuong += number;
        if(item.soLuong <= 0){
          for(var i =0; i< cloneCart.length; i++){
            if(cloneCart[i].id === id){
              cloneCart.splice(i,1);
            }
          }
        }
      }
    });

    this.setState({
      cart: cloneCart,
    });
  }
  handleRemoveShoe = (id) =>{
    // index

    let cloneCart =[...this.state.cart];
    for(var i =0; i< cloneCart.length; i++){
      if(cloneCart[i].id === id){
        cloneCart.splice(i,1);
      }
    }
    // splice(index,1)

    this.setState({
      cart: cloneCart,
    });
    console.log(id);
  }
  render() {
    // props.chidren : nội dung nằm giữ 2 thẻ đóng và mở
    return (
      <div className="container">
        <h2>{this.props.children}</h2>
        <Cart
        handleChaneQuantity ={this.handleChaneQuantity}
        handleRemoveShoe = {this.handleRemoveShoe}
        cart={this.state.cart} />
        <ListShoe
          handleAddToCart={this.handleAddToCart}


          list={this.state.listShoe}
        />
      </div>
    );
  }
}



import React, { Component } from 'react'
import { connect } from 'react-redux';

 class Redux_Ex extends Component {
  render() {
    console.log(this.props);
    return (
      <div>
        <button onClick={this.props.handleGiam} className='btn btn-success'>-</button>
        <strong  className='mx-5'>{this.props.soLuong}</strong>
        <button onClick={this.props.handleTang} className='btn btn-danger'>+</button>
      </div>
    )
  }
}


let mapStateToProps =(state) =>{
  return {
   soLuong : state.numberReducer.number,
  };
};

let mapDispatchTpProps = (dispatch)=>{
  return{
    handleTang :() =>{
      let action = {
        type : "TANG_SO_LUONG"
      };
      dispatch(action);
    },
    handleGiam:()=>{
      dispatch({
        type: "GIAM_SO_LUONG",
      })
    }
  }
}

export default connect(mapStateToProps,mapDispatchTpProps)(Redux_Ex);
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from "react-redux";
import { createStore } from "redux";
// import { rootReducer_DemoReduxMini } from "./DemoRedux_Mini/redux/reducer/rootReducer";
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { rootReducer_Redux_Ex } from './Redux_Ex/reducer/rootReducer';


const store = createStore(rootReducer_Redux_Ex,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
